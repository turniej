from django.conf.urls import url
from django.views.generic import TemplateView
from . import views


urlpatterns = [
    url(r'^$', views.home, name='e1_main_page'),
    url(r'^regulamin/$', TemplateView.as_view(template_name='edition1/rules.html'),
        name='e1_rules_page'),
    url(r'^wiecej/$', TemplateView.as_view(template_name='edition1/more.html'),
        name='e1_more_page'),
    url(r'^technikalia/$', TemplateView.as_view(template_name='edition1/technical.html'),
        name='e1_technical_page'),
]
