from django.shortcuts import render
from poetry.models import Poem

def home(request):
    last = Poem.objects.all().order_by('-created_at')[:10]
    return render(request, "edition1/home.html", locals())
