from django.conf.urls import include, url
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/2011/', permanent=False)),
    url(r'^2011/', include('edition1.urls')),
    url(r'^poezja/', include('poetry.urls')),
]
