from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^wiersz/(?P<slug>[a-zA-Z0-9-]+)/$', views.poem, name='poetry_poem'),

    url(r"^$", views.main, name="poetry_main"),
    url(r"^nowy/$", views.new, name="poetry_new"),
    url(r"^konkurs/$", views.contest, name="poetry_contest"),
    url(r"^(?P<slug>[a-zA-Z0-9-]+)/$", views.poet, name="poetry_poet"),
    url(r"^(?P<slug>[a-zA-Z0-9-]+)/nowy/$", views.new, name="poetry_new"),
]
