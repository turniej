import time

from base64 import urlsafe_b64encode
from hashlib import sha1


def get_hash(seed):
    sha_digest = sha1(('%d%s' % (time.time(), str(seed))
        ).encode('utf-8', 'replace')).digest()
    return urlsafe_b64encode(sha_digest).decode('latin1').replace('=', '').replace('_', '-').lower()
