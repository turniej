# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Poem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('slug', models.SlugField()),
                ('title', models.CharField(null=True, max_length=255, blank=True)),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField(db_index=True, auto_now_add=True)),
                ('seen_at', models.DateTimeField(auto_now_add=True)),
                ('view_count', models.IntegerField(default=1)),
                ('for_contest', models.BooleanField(default=False)),
                ('in_contest', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Poet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('path', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('author', models.CharField(max_length=128, blank=True)),
                ('url', models.CharField(max_length=255, blank=True)),
                ('description', models.TextField(blank=True)),
                ('first_line_title', models.BooleanField(default=False)),
                ('skip_first_lines', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='poem',
            name='poet',
            field=models.ForeignKey(to='poetry.Poet', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
